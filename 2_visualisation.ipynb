{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Visualisation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The core of these practical exercises focuses on posing essential questions about the dataset and creating visualizations to answer them. Notable visualizations include histograms of patient ages, bar charts of average heart rates by blood type, and scatter plots exploring correlations between weight and height.\n",
    "\n",
    "In order to create those vizualisations, three Python libraries are used: `matplotlib`, `pandas`, and `seaborn`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### MatPlotLib\n",
    "\n",
    "Matplotlib is a 2D plotting library for Python that enables the creation of static, animated, and interactive visualizations in Python. It provides a variety of charts and plots for visualizing data, making it a powerful tool for data exploration and presentation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Pandas DataFrame\n",
    "\n",
    "Pandas is a powerful data manipulation and analysis library for Python. One of its key features is the DataFrame, a two-dimensional, labeled data structure with columns that can be of different types (integer, float, string, etc.). DataFrames provide an efficient and easy-to-use data structure for handling and analyzing structured data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Seaborn\n",
    "\n",
    "Seaborn is a powerful data visualization library built on top of Matplotlib in Python. It provides an aesthetically pleasing and high-level interface for creating informative statistical graphics. Seaborn comes with several built-in themes and color palettes to make it easy to create visually appealing visualizations with minimal effort."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Both Matplotlib and Pandas are widely used in the data science and machine learning communities for data exploration, visualization, and analysis. They complement each other well, with Matplotlib handling the visualization aspect, and Pandas providing a convenient structure for manipulating and analyzing data. In addition, Seaborn complements Matplotlib and Pandas DataFrame by adding several enhancements and simplifying the process of creating complex statistical visualizations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Database connection"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The code begins by importing the `mariadb` module. This module provides a Python interface for interacting with MariaDB databases (fork of MySQL).\n",
    "\n",
    "The `mariadb.connect` function is used to establish a connection to a MariaDB database. It takes several parameters:\n",
    "- `host`: The hostname or IP address of the MariaDB server. In this case, it is set to `host.doors.internal`.\n",
    "- `user`: The username used to authenticate with the MariaDB server. Here, it is set to `student`.\n",
    "- `database`: The name of the database to connect to. It is set to `universite-grenoble-alpes_kotzkis`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "import mysql.connector as mariadb\n",
    "\n",
    "try:\n",
    "    connection = mariadb.connect(\n",
    "        host=\"host.doors.internal\",\n",
    "        user=\"student\",\n",
    "        database=\"universite-grenoble-alpes_kotzkis\"\n",
    "    )\n",
    "except Exception as e:\n",
    "    print(e)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Declarations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this practical session, we will leverage the `query_database()` function in our hands-on exploration of MariaDB databases, using three tables: `data-management_table1`, `data-management_table2`, and `data-management_table3`. Each of these tables shares a common field, `patient_id`, enabling us to establish relationships and glean insights across datasets."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Table 1:\n",
    "- This table captures essential demographic information about patients, including family names, gender, marital status, and date of birth.\n",
    "- Fields:\n",
    "  - `patient_id`: integer\n",
    "  - `family_name`: string\n",
    "  - `gender`: string either `male` or `female`\n",
    "  - `marital_status`: string either `married`, `divorced`, `widowed` or `single`\n",
    "  - `date_of_birth`: date with format `YYYY-mm-dd`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Table 2:\n",
    "- Focusing on health metrics, this table contains data related to patients' physical attributes, including weight, height, and vital signs such as respiratory and heart frequencies. Additionally, it includes information on creatinine clearance and blood type.\n",
    "- Fields:\n",
    "  - `patient_id`: integer\n",
    "  - `weight`: integer\n",
    "  - `height`: integer\n",
    "  - `creatinine_clearance`: integer\n",
    "  - `respiratory_freq`: integer\n",
    "  - `heart_freq`: integer\n",
    "  - `blood_type`: string either `O-`, `O+`, `A-`, `A+`, `B-`, `B+`, `AB-` or `AB+`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Table 3:\n",
    "- This table provides insights into lifestyle factors affecting patients, including indicators for sleep apnea, smoking habits, and alcohol consumption.\n",
    "- Fields:\n",
    "  - `patient_id`: integer\n",
    "  - `sleep_apnea`: tiny integer either `0` (false) or `1` (true)\n",
    "  - `smoker`: tiny integer either `0` (false) or `1` (true)\n",
    "  - `alcohol`: tiny integer either `0` (false) or `1` (true)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Throughout the practical exercises, you will use the `query_database()` function to formulate SQL queries that draw connections between these tables based on the shared `patient_id` field. This integrated approach will empower you to extract meaningful and comprehensive information from our MariaDB database."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "table1 = \"`data-management_table1`\"\n",
    "table2 = \"`data-management_table2`\"\n",
    "table3 = \"`data-management_table3`\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def query_database(connection, query):\n",
    "    \"\"\"\n",
    "        Executes a SQL query on the provided database connection and returns the result.\n",
    "\n",
    "        :param connection: connection object\n",
    "        :param query: string of an SQL query to be executed\n",
    "\n",
    "        :return: list containing the result of the query\n",
    "    \"\"\"\n",
    "    try:\n",
    "        # Create a cursor object to interact with the database\n",
    "        cursor = connection.cursor()\n",
    "\n",
    "        # Execute the SQL query\n",
    "        cursor.execute(query)\n",
    "\n",
    "        # Fetch all rows from the result set\n",
    "        result = cursor.fetchall()\n",
    "\n",
    "        # Close the cursor to release resources\n",
    "        cursor.close()\n",
    "\n",
    "        # Return the result of the query\n",
    "        return result\n",
    "\n",
    "    except Exception as e:\n",
    "        # If an exception occurs, print the error message and return a default result\n",
    "        print(e)\n",
    "        return [(None, None, None)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Questions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you progress through this practical session, you'll notice that some of the queries and functions include placeholders represented by three dots: `...`\n",
    "\n",
    "To successfully execute the provided code snippets, you must replace these dots with the correct or appropriate information, such as specific column names, table names, or other relevant parameters.\n",
    "\n",
    "When working with SQL queries, the dots represent segments where you should insert the desired columns, conditions, or grouping specifications based on the given database schema.\n",
    "\n",
    "Similarly, in plot functions, the dots indicate positions where you should provide the appropriate variables, column names, or customization options to tailor the visualization to their specific dataset.\n",
    "\n",
    "This practice ensures that the code aligns with the structure and content of their own databases and datasets, allowing for meaningful analyses and visual representations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following functions are part of the Matplotlib library and are commonly used to customize and display histograms and other plots.\n",
    "\n",
    "- `plt.hist(data, bins, color, edgecolor)` is used to create a histogram, which is a graphical representation of the distribution of a dataset. It divides the data into bins and displays the frequency of observations in each bin.\n",
    "\n",
    "- `plt.title(label)` is used to set the title of the plot. The label parameter is a string that represents the title of the plot. It provides context and information about the content of the plot.\n",
    "\n",
    "- `plt.xlabel(label)` and `plt.ylabel(label)` are used to set labels for the x-axis and y-axis, respectively. These labels provide information about the variables or data represented on each axis. The `label` parameter is a string specifying the label for the respective axis."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`Calculate the ages of patients` from their `date of birth` and create a `histogram` to visualize the age distribution. \n",
    "\n",
    "1. How is the age calculated from the date of birth? Why is the division by 365.25 used?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import math\n",
    "\n",
    "query = f\"SELECT DATEDIFF(NOW(), age.date_of_birth) FROM (SELECT ... FROM {...}) AS age;\"\n",
    "\n",
    "result = query_database(connection, query)\n",
    "\n",
    "# Calculate the age for each patient\n",
    "patients_age = [ math.floor(tuple[0]/365.25) for tuple in result ]\n",
    "\n",
    "# Create a histogram\n",
    "plt.hist(patients_age, bins=10, color='skyblue', edgecolor='black')\n",
    "plt.title('Age distribution of patients')\n",
    "plt.xlabel('...')\n",
    "plt.ylabel('...')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `pandas.DataFrame()`, or `pd.DataFrame()`, is a fundamental component of the pandas library in Python, serving as a powerful and flexible data structure for handling and manipulating tabular data. A DataFrame is essentially a two-dimensional, size-mutable, and potentially heterogeneous tabular data structure with labeled axes (rows and columns). It can be thought of as an in-memory, spreadsheet-like representation of data, making it immensely useful for data cleaning, exploration, and analysis.\n",
    "\n",
    "The DataFrame organizes data into rows and columns, where each column can have a specific data type (e.g., integer, float, string) and is labeled with a unique name. Rows are indexed, allowing for easy and intuitive referencing of individual data points. You can create a DataFrame from various data sources, including CSV files, SQL query results, and Excel spreadsheets. Once created, you can perform a wide range of operations on the DataFrame, such as filtering, sorting, grouping, and statistical computations, making it an indispensable tool in the data science and analysis toolkit."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`sns.barplot(x, y, data, hue, palette)` is a function provided by the Seaborn library, and it is used to create a bar plot. Seaborn is built on top of Matplotlib and provides a high-level interface for creating informative and attractive statistical graphics.\n",
    "\n",
    "- `x`: This parameter represents the data to be plotted on the x-axis. It can be a column name, array, or vector.\n",
    "- `y`: This parameter represents the data to be plotted on the y-axis. It can be a column name, array, or vector.\n",
    "- `data`: This parameter is the DataFrame or long-form data that contains the variables.\n",
    "- `hue` (optional): This parameter adds a categorical variable to the plot and produces color-coded bars. It can be a column name.\n",
    "- `palette` (optional): This parameter sets the color palette for the plot. It can be a string specifying a named seaborn palette or a list of colors."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Retrieve the `average heart rate for each blood type` from a database table and create a `bar chart` using the seaborn library with the `blood type on the x-axis` and the `heart frequency on the y-axis`.\n",
    "\n",
    "2. What insights can be derived from the average heart rates for different blood types?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import seaborn as sns\n",
    "\n",
    "query = f\"SELECT blood_type, ...(heart_freq) FROM {...} GROUP BY ...;\"\n",
    "\n",
    "result = query_database(connection, query)\n",
    "\n",
    "# Get the average heart rate for each blood type\n",
    "avg_heart_freq = pd.DataFrame(result, columns=['blood_type', 'heart_freq'])\n",
    "\n",
    "# Create a bar chart\n",
    "sns.barplot(x='...', y='...', data=..., palette='viridis', hue='blood_type')\n",
    "plt.title('Average heart rate per blood type')\n",
    "plt.xlabel('...')\n",
    "plt.ylabel('...')\n",
    "...()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `plt.scatter()` function in Matplotlib is used to create a scatter plot, which is a type of data visualization that displays individual data points on a two-dimensional graph. Each data point is represented by a marker on the graph, with its position determined by the values of two variables. This plot is particularly useful for visually identifying patterns, relationships, and trends in data.\n",
    "\n",
    "The function takes two main parameters: `the values for the x-axis and the y-axis`. These can be arrays, lists, or other iterable objects containing the numerical data you want to plot. Additionally, `plt.scatter()` provides several optional parameters that allow you to customize the appearance of the scatter plot, such as marker type, color, size, and transparency (i.e., alpha).\n",
    "\n",
    "Scatter plots are commonly used to investigate the correlation between two variables, helping to identify clusters, outliers, and the overall distribution of data points. They are widely employed in data exploration and analysis, especially when dealing with continuous or numerical data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Fetch data on patient `weight` and `height` from a database table and create a `scatter plot` to visualize `the correlation between weight and height`.\n",
    "\n",
    "3. How can the scatter plot be interpreted in terms of the correlation between patient weight and height?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "query = f\"SELECT ...;\"\n",
    "\n",
    "result = query_database(connection, query)\n",
    "\n",
    "patients_data = pd.DataFrame(result, columns=['...', '...'])\n",
    "\n",
    "# Create a scatter plot\n",
    "plt.scatter(patients_data['...'], patients_data['...'], alpha=0.5, color='green')\n",
    "plt.title('...')\n",
    "...('...')\n",
    "...('...')\n",
    "...()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `dataframe.plot.pie()` method in pandas is used to create a pie chart from the data stored in a DataFrame. A pie chart is a circular statistical graphic that is divided into slices to illustrate numerical proportions. Each slice represents a proportionate part of the whole data set, and the size of each slice is determined by the corresponding values in the DataFrame.\n",
    "\n",
    "The primary parameters for this method include:\n",
    "\n",
    "- `y`: The column name or index of the DataFrame containing the data for which the pie chart will be created. This is typically a numeric column representing the proportions.\n",
    "- `labels`: An optional parameter specifying the labels for each pie slice. If not provided, the index or column names will be used.\n",
    "- `autopct`: An optional parameter that controls the display of percentage values on the pie slices. Setting it to `'%1.1f%%'` would display percentages rounded to one decimal place.\n",
    "\n",
    "This method generates a pie chart using Matplotlib, and you can further customize the appearance of the chart by leveraging additional Matplotlib parameters. Pie charts are often used to visually represent the distribution of categories within a dataset and provide an easy way to understand the relative proportions of different parts in a whole."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Retrieve the `count of each blood type` from a database table and create a `pie chart` to represent the `distribution of blood types`.\n",
    "\n",
    "4. Why is a pie chart chosen to represent the distribution of blood types?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "query = f\"SELECT blood_type, ...(*) FROM {...} GROUP BY ...;\"\n",
    "\n",
    "result = query_database(connection, query)\n",
    "\n",
    "# Get the count of each blood type\n",
    "blood_type_counts = pd.DataFrame(result, columns=['...', 'count'])\n",
    "\n",
    "# Create a pie chart\n",
    "blood_type_counts.plot.pie(y='...', labels=blood_type_counts['...'], autopct='%1.1f%%', startangle=90, colors=plt.cm.tab20.colors)\n",
    "plt.title('...')\n",
    "plt.ylabel('') # Hide label y\n",
    "plt.legend(bbox_to_anchor=(1, 1), loc='upper left')\n",
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `dataframe.plot()` method in pandas serves as a convenient interface to Matplotlib. When applied to a DataFrame, this method allows you to generate different types of plots, including line plots, bar plots, scatter plots, and more, depending on the nature of your data.\n",
    "\n",
    "- `kind`: Specifies the type of plot you want to create such as `line`, `bar`, `barh`, `hist`, `box`, and many more.\n",
    "- `x` and `y`: Specify the DataFrame columns to be used as the x and y axes.\n",
    "\n",
    "One specific parameter that can be used with the `dataframe.plot()` method is `stacked`. When `stacked` is set to `True`, it is applicable to certain plot types like bar plots, and it results in stacking the values of multiple columns on top of each other. This is particularly useful when you want to compare the total values and the contribution of each column to the total."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Retrieve data on the `smoking and alcohol status` of patients and create a `stacked bar chart` to show `the proportion of smokers and alcohol consumers`.\n",
    "\n",
    "5.  What insights can be gained from the stacked bar chart regarding the relationship between smoking, alcohol consumption, and the number of patients?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "query = f\"...;\"\n",
    "\n",
    "result = query_database(connection, query)\n",
    "\n",
    "# Get the number of patients grouped by categories\n",
    "smoker_alcohol_counts = pd.DataFrame(result, columns=['...', '...']).groupby(['smoker', 'alcohol']).size().unstack()\n",
    "\n",
    "# Create a stacked bar chart\n",
    "smoker_alcohol_counts.plot(kind='...', stacked=..., color=['red', 'blue'])\n",
    "\n",
    "# Title, xlable, ylable\n",
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `sns.boxplot()` function in Seaborn is used to create a box-and-whisker plot, which is a standardized way of displaying the distribution of data based on a five-number summary: minimum, first quartile (Q1), median, third quartile (Q3), and maximum. Seaborn builds on Matplotlib but provides a higher-level interface for creating aesthetically pleasing statistical graphics.\n",
    "\n",
    "- `x` and `y`: Represent the variables to be plotted on the x and y axes, respectively.\n",
    "- `data`: The DataFrame containing the data.\n",
    "- `hue`: Optional parameter that allows to differentiate subsets of the data by a categorical variable."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Fetche data on `creatinine clearance` from a database table and create a `boxplot` to visualize its distribution.\n",
    "\n",
    "6. How can the boxplot be interpreted in terms of the distribution of creatinine clearance among patients?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "query = f\"...;\"\n",
    "\n",
    "result = query_database(connection, query)\n",
    "\n",
    "creatinine_clearance = pd.DataFrame(result, columns=['...'])\n",
    "\n",
    "# Create a boxplot\n",
    "sns.set_style('whitegrid')\n",
    "sns.boxplot(data=..., y='...')\n",
    "\n",
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Closing the database connection"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In database programming, `connection.close()` is a method used to close the connection between the application and the database. When a database connection is established, it consumes system resources. Closing the connection is essential to free up these resources and ensure efficient use of system memory.\n",
    "\n",
    "It is considered a best practice to close the database connection as soon as it's no longer needed, typically after executing the required queries or transactions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "connection.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusion"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This practical exercise provided a hands-on exploration of data visualization using Python libraries such as Matplotlib, Pandas, and Seaborn. These libraries offer powerful tools for creating informative and visually appealing visualizations, enabling the extraction of meaningful insights from datasets.\n",
    "\n",
    "The exercise covered essential concepts, including the use of Matplotlib for static visualizations, Pandas for data manipulation through DataFrames, and Seaborn for high-level statistical graphics. The integration of these libraries facilitates comprehensive data exploration, analysis, and presentation.\n",
    "\n",
    "In summary, this practical exercise provided a learning experience, combining database querying skills with data visualization techniques to extract valuable insights from a healthcare dataset, together encouraging to further explore and apply these tools in their own projects and analyses."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
