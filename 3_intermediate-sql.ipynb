{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Intermediate SQL"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this practical session you are going to deal specifically with missing or incorrect values in a database. The goal is to show you how to handle these issues while doing some statistical exercises.\n",
    "\n",
    "When you're working with databases, you often run into situations where the data isn't perfect – it might be missing some bits or have some errors. That's where SQL comes in handy. Today, we'll walk through how to spot these problems and fix them using straightforward SQL queries.\n",
    "\n",
    "The focus here is on real-life scenarios, where databases don't have all the info you need. By using SQL, you'll learn how to tidy up the data so you can trust the numbers when you're doing statistical stuff."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "09d35c51-3c34-4454-98c9-65161364a317",
   "metadata": {},
   "source": [
    "## Database connection"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The code begins by importing the `mariadb` module. This module provides a Python interface for interacting with MariaDB databases (fork of MySQL).\n",
    "\n",
    "The `mariadb.connect` function is used to establish a connection to a MariaDB database. It takes several parameters:\n",
    "- `host`: The hostname or IP address of the MariaDB server. In this case, it is set to `host.doors.internal`.\n",
    "- `user`: The username used to authenticate with the MariaDB server. Here, it is set to `student`.\n",
    "- `database`: The name of the database to connect to. It is set to `universite-grenoble-alpes_kotzkis`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "085c3440-5b68-4dcd-a4ee-c0deeed9b993",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "import mysql.connector as mariadb\n",
    "\n",
    "try:\n",
    "    connection = mariadb.connect(\n",
    "        host=\"host.doors.internal\",\n",
    "        user=\"student\",\n",
    "        database=\"universite-grenoble-alpes_kotzkis\"\n",
    "    )\n",
    "except Exception as e:\n",
    "    print(e)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "19d8b502-c546-4ea6-b484-34eefc0a23e3",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Declarations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this practical session, we will leverage the `query_database()` function in our hands-on exploration of MariaDB databases, using six tables: `data-management_table1`, `data-management_table1_complex`, `data-management_table2`, `data-management_table2_null`, `data-management_table3`, and `data-management_table3_invalid`. Each of these tables shares a common field, `patient_id`, enabling us to establish relationships and glean insights across datasets."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Table 1:\n",
    "- This table captures essential demographic information about patients, including family names, gender, marital status, and date of birth.\n",
    "- Fields:\n",
    "  - `patient_id`: integer\n",
    "  - `family_name`: string\n",
    "  - `gender`: string either `male` or `female`\n",
    "  - `marital_status`: string either `married`, `divorced`, `widowed` or `single`\n",
    "  - `date_of_birth`: date with format `YYYY-mm-dd`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Table 1 (complex):\n",
    "- Same table as table 1, but with some wrong `date_of_birth` values (e.g., birth dates in the future)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Table 2:\n",
    "- Focusing on health metrics, this table contains data related to patients' physical attributes, including weight, height, and vital signs such as respiratory and heart frequencies. Additionally, it includes information on creatinine clearance and blood type.\n",
    "- Fields:\n",
    "  - `patient_id`: integer\n",
    "  - `weight`: integer\n",
    "  - `height`: integer\n",
    "  - `creatinine_clearance`: integer\n",
    "  - `respiratory_freq`: integer\n",
    "  - `heart_freq`: integer\n",
    "  - `blood_type`: string either `O-`, `O+`, `A-`, `A+`, `B-`, `B+`, `AB-` or `AB+`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Table 2 (null):\n",
    "- Same table as table 2, but with some missing `height` values."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Table 3:\n",
    "- This table provides insights into lifestyle factors affecting patients, including indicators for sleep apnea, smoking habits, and alcohol consumption.\n",
    "- Fields:\n",
    "  - `patient_id`: integer\n",
    "  - `sleep_apnea`: tiny integer either `0` (false) or `1` (true)\n",
    "  - `smoker`: tiny integer either `0` (false) or `1` (true)\n",
    "  - `alcohol`: tiny integer either `0` (false) or `1` (true)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Table 3 (invalid):\n",
    "- Same table as table 3, but with some invalid `smoker` boolean values (i.e., values other than 0 or 1)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Throughout the practical exercises, you will use the `query_database()` function to formulate SQL queries that draw connections between these tables based on the shared `patient_id` field. This integrated approach will empower you to extract meaningful and comprehensive information from our MariaDB database."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e2d5a4bf-6304-49b7-9701-08584ccf7da3",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "table1 = \"`data-management_table1`\"\n",
    "table1_complex = \"`data-management_table1_complex`\"\n",
    "table2 = \"`data-management_table2`\"\n",
    "table2_null = \"`data-management_table2_null`\"\n",
    "table3 = \"`data-management_table3`\"\n",
    "table3_invalid = \"`data-management_table3_invalid`\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4c14d491-45be-4c43-a151-6e0abba6d66d",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "def query_database(connection, query):\n",
    "    \"\"\"\n",
    "        Executes a SQL query on the provided database connection and returns the result.\n",
    "\n",
    "        :param connection: connection object\n",
    "        :param query: string of an SQL query to be executed\n",
    "\n",
    "        :return: list containing the result of the query\n",
    "    \"\"\"\n",
    "    try:\n",
    "        # Create a cursor object to interact with the database\n",
    "        cursor = connection.cursor()\n",
    "\n",
    "        # Execute the SQL query\n",
    "        cursor.execute(query)\n",
    "\n",
    "        # Fetch all rows from the result set\n",
    "        result = cursor.fetchall()\n",
    "\n",
    "        # Close the cursor to release resources\n",
    "        cursor.close()\n",
    "\n",
    "        # Return the result of the query\n",
    "        return result\n",
    "\n",
    "    except Exception as e:\n",
    "        # If an exception occurs, print the error message and return a default result\n",
    "        print(e)\n",
    "        return [(None, None, None, None, None)]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "52ac5e8e-c871-4532-9c22-033d65b6e558",
   "metadata": {},
   "source": [
    "## Questions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you progress through this practical session, you'll notice that some of the queries include placeholders represented by three dots: `...`\n",
    "\n",
    "It is crucial to actively engage with the exercises by replacing each set of three dots with the correct or appropriate words to formulate valid and meaningful SQL queries. This process is designed to enhance your understanding of query construction and ensure that you can apply your knowledge in real-world scenarios.\n",
    "\n",
    "Take a moment to carefully analyze the context of each query and replace the placeholders with the appropriate keywords, table names, or conditions. This hands-on approach will not only reinforce your SQL skills but also empower you to confidently interact with databases in your future endeavors."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`PERCENTILE_CONT()` is a function in MariaDB used for calculating a specific percentile for a set of values in a specified column. This function is particularly useful in statistical analysis and is often employed to find the value below which a given percentage of observations in a group fall.\n",
    "\n",
    "Example: `PERCENTILE_CONT(percentile) WITHIN GROUP (ORDER BY column_name)`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`IS NOT NULL` is a condition in SQL used to filter rows in a query result where a specific column contains non-null (i.e., not empty or undefined) values. This condition is often used in the WHERE clause of a SQL query to exclude rows where a particular column has a null value."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21b5121c-9b0e-483c-a3be-eec07f1563c9",
   "metadata": {},
   "source": [
    "1. What is the `median height` of the patients? Use the relevant SQL functions to handle cases where the height values are missing."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "653cba22-c923-4bb1-a51f-0b17103d53e3",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "query = f\"SELECT PERCENTILE_CONT(...) WITHIN GROUP (ORDER BY ...) OVER () AS p_id \\\n",
    "            FROM {table2_null} \\\n",
    "            WHERE ...;\"\n",
    "\n",
    "result = query_database(connection, query)\n",
    "print(result[0][0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`STDDEV_POP()` is a statistical aggregate function in SQL that calculates the population standard deviation of a set of values. Standard deviation is a measure of the amount of variation or dispersion in a set of values. The \"POP\" in `STDDEV_POP()` stands for \"population,\" indicating that it computes the standard deviation for the entire population rather than a sample.\n",
    "\n",
    "Example: `SELECT STDDEV_POP(column_name)`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c3e140b8-787a-4043-96d1-8bd215de93de",
   "metadata": {},
   "source": [
    "2. Calculate the `standard deviation` of the `weight of all patients`. How would you interpret this value in the context of patients' health?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fae6f7eb-40b7-452e-85cd-7dcb079ab9a3",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "query = f\"SELECT ... FROM {table2};\"\n",
    "\n",
    "result = query_database(connection, query)\n",
    "print(result[0][0])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0501132e-da8e-4166-bf44-68e40291c75f",
   "metadata": {},
   "source": [
    "3. Write an SQL query that calculates the `average age of patients` from `table 1` and `table 2`, broken down by `marital status and blood type`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "70a4577c-3e8b-4275-b3f3-8ac52bb18b64",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "query = f\"SELECT ..., ..., ...(YEAR(CURDATE()) - YEAR(date_of_birth)) AS average_age \\\n",
    "            FROM {...} \\\n",
    "            JOIN ... \\\n",
    "            ...;\"\n",
    "\n",
    "result = query_database(connection, query)\n",
    "print(\"marital_status\\tblood_type\\taverage_age\")\n",
    "for tuple in result:\n",
    "    print(f\"{tuple[0]}\\t{tuple[1]}\\t{tuple[2]}\".expandtabs(16))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Table aliases in SQL are used to provide short, alternative names for tables in a query. They make the SQL code more concise and readable, especially when dealing with multiple tables in a query.\n",
    "\n",
    "Example: `SELECT t1.column_name FROM table1 t1 JOIN table2 t2 ON t1.column_name = t2.column_name`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c5bfd501-480d-41fa-be0e-7140c000a6a1",
   "metadata": {},
   "source": [
    "4. Build a query that returns the `patient ID`, the `family name` and the `age` of `three youngest patients` who are `active smokers` using `table 1` and `table 3`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "59215232-e85f-4554-8aa2-4b50c22825de",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "query = f\"SELECT ..., ..., YEAR(CURDATE()) - YEAR(t1.date_of_birth) AS age \\\n",
    "            FROM {table1} t1 \\\n",
    "            JOIN {...} ... \\\n",
    "            WHERE ... \\\n",
    "            ORDER BY t1.date_of_birth ... \\\n",
    "            LIMIT ...;\"\n",
    "\n",
    "result = query_database(connection, query)\n",
    "print(\"patient_id\\tfamily_name\\tage\")\n",
    "for tuple in result:\n",
    "    print(f\"{tuple[0]}\\t{tuple[1]}\\t{tuple[2]}\".expandtabs(16))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ffdf6b57-4d10-4a96-a300-9d44b31b72f6",
   "metadata": {
    "tags": []
   },
   "source": [
    "5. Identify records with `missing` or `inconsistent data`, such as `future dates of birth`, and propose a method for handling them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f2a80147-6d5e-46a9-a2a1-cb03adb1718c",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "query = f\"SELECT * FROM {table1_complex} WHERE ...;\"\n",
    "\n",
    "result = query_database(connection, query)\n",
    "print(\"patient_id\\tfamily_name\\tgender\\tmarital_status\\tdate_of_birth\".expandtabs(16))\n",
    "for tuple in result:\n",
    "    print(f\"{tuple[0]}\\t{tuple[1]}\\t{tuple[2]}\\t{tuple[3]}\\t{tuple[4]}\".expandtabs(16))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `CASE WHEN ... THEN ... ELSE ... END` statement in SQL is a powerful and flexible way to perform conditional logic within a query. It allows you to create conditional expressions, assigning different values or performing various actions based on specified conditions.\n",
    "\n",
    "Example: `CASE\n",
    "            WHEN condition1 THEN result1\n",
    "            WHEN condition2 THEN result2\n",
    "            ...\n",
    "            ELSE default_result\n",
    "          END AS alias_name`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "06fbd8bb-48de-451a-b431-4371eb22803b",
   "metadata": {},
   "source": [
    "6. Suppose that for some patients, the `smoker` field is `incorrectly filled with values other than TRUE or FALSE`. Write an SQL query to find their `patient ID`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "de4ec12e-b3f2-4dae-bbf4-4cce4bd9e17f",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "query = f\"SELECT ..., CASE ... FROM {table3_invalid};\"\n",
    "\n",
    "result = query_database(connection, query)\n",
    "print(\"patient_id\\tsmoker\")\n",
    "# Print the 10 first rows\n",
    "for tuple in result[:10]:\n",
    "    print(f\"{tuple[0]}\\t{tuple[1]}\".expandtabs(16))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `LEFT()` and `RIGHT()` functions in SQL are used to extract a specified number of characters from the left or right side of a string, respectively. These functions are particularly useful when you want to manipulate or display a portion of a string column."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b95f5fa9-a33e-4f11-8fe7-c36305206a09",
   "metadata": {},
   "source": [
    "7. Imagine that the `family_name` column contains sensitive information. Create a query to select `all information of every patient without displaying their complete information`, like `showing only the first letter`?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "02b9017f-af2f-4a31-ae8b-17c1d970c4f9",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "query = f\"SELECT ... FROM {table1_complex};\"\n",
    "\n",
    "result = query_database(connection, query)\n",
    "print(\"patient_id\\tfamily_name\\tgender\\tmarital_status\\tdate_of_birth\".expandtabs(16))\n",
    "# Print the 10 first rows\n",
    "for tuple in result[:10]:\n",
    "    print(f\"{tuple[0]}\\t{tuple[1]}\\t{tuple[2]}\\t{tuple[3]}\\t{tuple[4]}\".expandtabs(16))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Closing the database connection"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In database programming, `connection.close()` is a method used to close the connection between the application and the database. When a database connection is established, it consumes system resources. Closing the connection is essential to free up these resources and ensure efficient use of system memory.\n",
    "\n",
    "It is considered a best practice to close the database connection as soon as it's no longer needed, typically after executing the required queries or transactions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0a451ba5-72d5-436b-a1a3-31b9e6192a7e",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "connection.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusion"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Throughout this exercise, you delved into the practical aspects of working with databases, specifically addressing challenges related to missing or incorrect values. By using SQL queries, you've learned how to handle real-world scenarios where data isn't perfect, ensuring the reliability of statistical exercises."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
