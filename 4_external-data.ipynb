{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# External data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this exercise, you will be working with two datasets: a table stored in a database and a CSV file. The goal is to use SQL queries to retrieve a table from the database and to use Python's pandas library to join this table with the information from the CSV file. Finally, you will perform some statistical analysis on the resulting dataset."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- The table you will be working with contains information about patients, including their ID, weight, height, creatinine clearance, respiratory frequency, heart frequency, and blood type.\n",
    "\n",
    "- The CSV file includes additional information about patients' weights and corresponding dates. This data is an example of information retrieved from connected scales that patients used each week."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Database connection"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The code begins by importing the `mariadb` module. This module provides a Python interface for interacting with MariaDB databases (fork of MySQL).\n",
    "\n",
    "The `mariadb.connect` function is used to establish a connection to a MariaDB database. It takes several parameters:\n",
    "- `host`: The hostname or IP address of the MariaDB server. In this case, it is set to `host.doors.internal`.\n",
    "- `user`: The username used to authenticate with the MariaDB server. Here, it is set to `student`.\n",
    "- `database`: The name of the database to connect to. It is set to `universite-grenoble-alpes_kotzkis`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "import mysql.connector as mariadb\n",
    "\n",
    "try:\n",
    "    connection = mariadb.connect(\n",
    "        host=\"host.doors.internal\",\n",
    "        user=\"student\",\n",
    "        database=\"universite-grenoble-alpes_kotzkis\"\n",
    "    )\n",
    "except Exception as e:\n",
    "    print(e)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Declarations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this practical session, we will leverage the `query_database()` function in our hands-on exploration of MariaDB databases, using one table: `data-management_table2`. Each of these tables shares a common field, `patient_id`, enabling us to establish relationships and glean insights across datasets."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Table 2:\n",
    "- Focusing on health metrics, this table contains data related to patients' physical attributes, including weight, height, and vital signs such as respiratory and heart frequencies. Additionally, it includes information on creatinine clearance and blood type.\n",
    "- Fields:\n",
    "  - `patient_id`: integer\n",
    "  - `weight`: integer\n",
    "  - `height`: integer\n",
    "  - `creatinine_clearance`: integer\n",
    "  - `respiratory_freq`: integer\n",
    "  - `heart_freq`: integer\n",
    "  - `blood_type`: string either `O-`, `O+`, `A-`, `A+`, `B-`, `B+`, `AB-` or `AB+`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Throughout the practical exercises, you will use the `query_database()` function to formulate SQL queries that draw connections between these tables based on the shared `patient_id` field. This integrated approach will empower you to extract meaningful and comprehensive information from our MariaDB database."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "table2 = \"`data-management_table2`\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "def query_database(connection, query):\n",
    "    \"\"\"\n",
    "        Executes a SQL query on the provided database connection and returns the result.\n",
    "\n",
    "        :param connection: connection object\n",
    "        :param query: string of an SQL query to be executed\n",
    "\n",
    "        :return: list containing the result of the query\n",
    "    \"\"\"\n",
    "    try:\n",
    "        # Create a cursor object to interact with the database\n",
    "        cursor = connection.cursor()\n",
    "\n",
    "        # Execute the SQL query\n",
    "        cursor.execute(query)\n",
    "\n",
    "        # Fetch all rows from the result set\n",
    "        result = cursor.fetchall()\n",
    "\n",
    "        # Close the cursor to release resources\n",
    "        cursor.close()\n",
    "\n",
    "        # Return the result of the query\n",
    "        return result\n",
    "\n",
    "    except Exception as e:\n",
    "        # If an exception occurs, print the error message and return a default result\n",
    "        print(e)\n",
    "        return [(None, None, None)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data preparation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you progress through this practical session, you'll notice that some of the queries and functions include placeholders represented by three dots: `...`\n",
    "\n",
    "To successfully execute the provided code snippets, you must replace these dots with the correct or appropriate information, such as specific column names, table names, or other relevant parameters.\n",
    "\n",
    "When working with SQL queries, the dots represent segments where you should insert the desired columns, conditions, or grouping specifications based on the given database schema.\n",
    "\n",
    "Similarly, in plot functions, the dots indicate positions where you should provide the appropriate variables, column names, or customization options to tailor the visualization to their specific dataset.\n",
    "\n",
    "This practice ensures that the code aligns with the structure and content of their own databases and datasets, allowing for meaningful analyses and visual representations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`pandas.read_csv()` is a powerful function in the pandas library used for reading and loading data from a CSV file into a pandas DataFrame. It provides various options to customize the reading process based on the structure and characteristics of the CSV file.\n",
    "\n",
    "Some parameters for this function are:\n",
    "- `sep`: Delimiter to use for separating fields. The default is \",\".\n",
    "- `delimiter`: Alternative argument for specifying the delimiter.\n",
    "- `header`: Row(s) to use as the column names. The default is 'infer', which uses the first row as headers.\n",
    "- `names`: List of column names to use. If specified, it overrides the header."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "1. Import the external data from connected scales recorded in the file `connected_scales_incomplete.csv`, which is itself in the folder `data`. Data in the external file has \"`;`\" as a delimiter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "\n",
    "# Read the CSV file with specification of the value delimiter\n",
    "data = pd.read_csv('...', ...=...)\n",
    "\n",
    "# Convert the 'date' column to the datetime data type\n",
    "data['date'] = pd.to_datetime(data['date'])\n",
    "\n",
    "print(data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Patients won't always fully follow their schedule and their prescriptions. In the data you imported from the file `connected_scales_incomplete.csv`, you might have missing values in the `weight` column, often represented as \"`NaN`\" (Not a Number). It is important to address these missing values for a more complete and accurate analysis.\n",
    "\n",
    "Instead of just filling in missing values with a constant or arbitrary value, it is often beneficial to replace them with a value that reflects the overall pattern of the data. In this case, you will use the average weight of each patient as the replacement (e.g., missing weight values for patient 1 will be replaced with the average weight of patient 1).\n",
    "\n",
    "The average weight is a representative measure of central tendency for a set of weights. When you replace missing values with the average weight of a patient, you are incorporating the typical weight of that specific patient. Using the average helps to maintain the overall distribution and characteristics of the weight data for each patient. This is important for accurate statistical analysis."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2. First retrieve the average weight for each patient and store it as a DataFrame in the `average_weight_per_patient` variable.\n",
    "\n",
    "**Read and run the following cell, and understand its output.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Group the data by 'patient_id' and calculate the average weight for each patient, then convert it into a DataFrame\n",
    "average_weight_per_patient = data.groupby('patient_id')['weight'].mean().reset_index(name='average_weight')\n",
    "\n",
    "print(average_weight_per_patient)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dictionaries, unlike sequences such as lists or tuples, are unordered collections of items. They use a key-value pair system for organizing and accessing data, meaning that you can access values using their corresponding keys. This allows for rapid retrieval of data without the need to iterate through the entire collection.\n",
    "\n",
    "Example:\n",
    "\n",
    "```\n",
    "dict = {\n",
    "        'name': 'John',\n",
    "        'age': 25,\n",
    "        'city': 'London'\n",
    "       }\n",
    "\n",
    "var = dict['name']\n",
    "# var now contains the string 'John'\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "3. Now replace each missing weight value in `data` with the collected average weight for each patient.\n",
    "\n",
    "**Read and run the following cell, and compare its output with the output of question 1.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Create a dictionnary with 'patient_id' as key and 'average_weight' as value to those keys\n",
    "weight_mapping = average_weight_per_patient.set_index('patient_id')['average_weight'].to_dict()\n",
    "\n",
    "average_weight_per_patient.reset_index()\n",
    "\n",
    "# Replace missing weight values using the dictionnary\n",
    "data['weight'] = data.apply(\n",
    "    lambda row: weight_mapping.get(row['patient_id'], row['weight'])\n",
    "        if pd.isna(row['weight']) else row['weight'],\n",
    "    axis=1)\n",
    "\n",
    "print(data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By comparing the output of the previous question, which involved grouping the data by `patient ID` and calculating the average weight for each patient, with the output of question 1, you can observe a meaningful transformation in the dataset. Specifically, missing values represented as \"`NaN`\" in the `weight` column have been effectively replaced with the calculated average weight for each respective patient.\n",
    "\n",
    "In the modified dataset, the `weight` column now reflects a more comprehensive and informative representation of patient weights. The process involved replacing missing values with a more contextually relevant measure—the average weight for each patient group. This approach enhances the completeness of the data and ensures that statistical analyses and interpretations are based on a more accurate and representative dataset."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "4. Retrieve the weights from table 2 and update the previously imported external data with these weights.\n",
    "\n",
    "**Read and run the following cell, and understand its output.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "from datetime import datetime\n",
    "\n",
    "# Get all data from table 2\n",
    "query = f\"SELECT * FROM {table2};\"\n",
    "result = query_database(connection, query)\n",
    "\n",
    "# Get column names for table 2\n",
    "query_columns = f\"SHOW COLUMNS FROM {table2};\"\n",
    "result_columns = query_database(connection, query_columns)\n",
    "columns = [ column_information[0] for column_information in result_columns ]\n",
    "\n",
    "# Create a DataFrame from the previously treated connected scales data\n",
    "data_df = pd.DataFrame(data)\n",
    "\n",
    "# Create a DataFrame from table 2\n",
    "table2_df = pd.DataFrame(result, columns=columns)\n",
    "\n",
    "# Concatenate the weights from the treated connected scales data to the weights from table 2, ignoring DataFrame's index\n",
    "data_updated = pd.concat([data_df, table2_df[['patient_id', 'weight']]], ignore_index=True)\n",
    "\n",
    "print(data_updated)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When you concatenate `table 2` with the `connected scales` `data` table, it combines the two tables along a specified axis. In this case, since `table 2` doesn't contain any date information, the concatenation creates missing date values represented as \"`NaT`\" (Not a Time) in the resulting combined table.\n",
    "\n",
    "The next step involves addressing these missing date values. To ensure the completeness of the data, it is necessary to replace \"`NaT`\" with valid date values in the format \"`%Y-%m-%d`\".\n",
    "\n",
    "For replacing missing dates, the datetime library in Python offers various functions. Here are a few options:\n",
    "\n",
    "- `datetime.min`: The minimum representable datetime object (datetime.datetime(1, 1, 1, 0, 0)).\n",
    "- `datetime.max`: The maximum representable datetime object (datetime.datetime(9999, 12, 31, 23, 59, 59, 999999)).\n",
    "- `datetime.today()`: Provides the current date and time. However, note that it includes the time component, and you might want to extract only the date.\n",
    "- `datetime.strptime(date_string, format)`: Parses a string representing a date and time according to the specified format and returns a datetime object.\n",
    "- `datetime.timedelta(days=0, seconds=0, microseconds=0, milliseconds=0, minutes=0, hours=0, weeks=0)`: Represents a duration or the difference between two dates or times."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "5. The `table 2` does not contain any date, which created missing date values \"`NaT`\". Replace these missing date values with a date of your choice using one of the `datetime` functions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Get a date with the appropriate format\n",
    "replacement_date = datetime.....strftime('%Y-%m-%d')\n",
    "\n",
    "# Replace missing dates with the replacement date\n",
    "data_updated['date'] = data_updated['date'].fillna(replacement_date)\n",
    "\n",
    "print(data_updated)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sorting the concatenated data first by `patient ID` and then by `date` is essential for correct visualization and analysis.\n",
    "\n",
    "Sorting by `patient ID` first logically groups the data by patients. This arrangement is particularly important when dealing with time-series data related to individual patients. It allows you to view and analyze the data for each patient in a contiguous block, making it easier to identify patterns, trends, or anomalies specific to each individual.\n",
    "\n",
    "Sorting by `date` within each `patient ID` group ensures that the data for each patient is arranged chronologically. This is crucial when dealing with time-stamped data, such as measurements taken at different points in time. It allows for a clear understanding of the temporal order of events for each patient.\n",
    "\n",
    "If you were to sort by `date` first and then by `patient ID`, the result would be a chronological sequence of all data points across all patients. This order could make it challenging to distinguish individual patient trends or compare different patients effectively."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "6. Now, sort the updated `data` by `patient_id` and then by `date`.\n",
    "\n",
    "**Compare the following output with the output of the previous question.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "data_updated = data_updated.sort_values(['...','...'])\n",
    "\n",
    "print(data_updated)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Graphical visualisation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that the data from the `connected scales` has been updated with the information from `table 2` of the database, you have a comprehensive dataset that incorporates both the original measurements and the additional weights obtained from connected scales. This updated dataset allows you to visualize the combined data effectively and derive meaningful statistics."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "7. Display the `updated data` from the `connected scales` by grouping the `date` column and the `weight` column for each `patient_id`. What insight can you make out of the resulting graph?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "\n",
    "plt.figure(figsize=(10, 6))\n",
    "\n",
    "# Iterate over each group of values grouped by patient_id and plot each of them (by default, with a different color)\n",
    "for patient_id, group in data_updated.groupby('...'):\n",
    "    plt.plot(group['...'], group['...'])\n",
    "\n",
    "plt.grid(True)\n",
    "plt.title('...')\n",
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A DataFrame has many functions which all allow to automatically retrieve meaningful information from its data.\n",
    "\n",
    "- `dataframe.describe()`: Generates descriptive statistics, including mean, median, and quartiles, for numeric columns.\n",
    "- `dataframe.mean()`: Calculate the mean or average of values.\n",
    "- `dataframe.median()`: Computes the median value for each numeric column.\n",
    "- `dataframe.min()`, `dataframe.max()`: Determine the minimum and maximum values for each numeric column, respectively.\n",
    "- `dataframe.std()`: Computes the standard deviation for each numeric column.\n",
    "- `dataframe.sum()`: Calculates the sum of values for each numeric column.\n",
    "- `dataframe.var()`: Calculates the variance for each numeric column."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "8. Display the `average weight` for a patient of your chosing."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patient_id = ...\n",
    "average_weight = data_updated[data_updated['...'] == patient_id]['...']....\n",
    "\n",
    "print(average_weight)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "9. Use the `average_weight_per_patient` variable, which contains the `average weights` column for each patient, and use it to display a `box plot` of quartiles. What insight can you give from the resulting box plot?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(8, 6))\n",
    "plt.boxplot(average_weight_per_patient['...'], vert=True)\n",
    "plt.title('...')\n",
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Closing the database connection"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In database programming, `connection.close()` is a method used to close the connection between the application and the database. When a database connection is established, it consumes system resources. Closing the connection is essential to free up these resources and ensure efficient use of system memory.\n",
    "\n",
    "It is considered a best practice to close the database connection as soon as it's no longer needed, typically after executing the required queries or transactions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "connection.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusion"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this practical exercise, you engaged in a comprehensive exploration of data management and analysis using both SQL queries and Python's pandas library. The exercise involved two datasets: one stored in a database table and the other in a CSV file. The primary objectives were to retrieve data from the database, join it with external data, and perform statistical analysis.\n",
    "\n",
    "The exercise began with establishing a connection to the MariaDB database and declaring necessary variables. The data preparation phase included reading the external CSV file, converting date columns to datetime type, and addressing missing weight values by replacing them with the average weight of each patient.\n",
    "\n",
    "You learned about the importance of using the average weight as a replacement for missing values, as it maintains the overall distribution and characteristics of the data for each patient, contributing to accurate statistical analyses.\n",
    "\n",
    "The importance of sorting the data by patient ID and then by date was highlighted for effective visualization and analysis. Finally, graphical visualization allowed you to observe the trends in the updated dataset, and statistical functions provided some insights into the average weights for individual patients.\n",
    "\n",
    "Remember, this practical session provides practical insights into real-world scenarios where data integration, cleaning, and analysis are crucial for deriving meaningful conclusions. The skills acquired here are applicable in various domains where data plays a central role."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you have further questions or encounter challenges, don't hesitate to reach out for support.\n",
    "\n",
    "Farewell and best of luck to all the students who have completed this practical session!"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
